﻿using OpenTK;
using System.Drawing;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTKBase
{
    /// <summary>
    /// The GameClient class is where all your in game code should be done.
    /// If another game state besides "in-game" is added, you should make a different client class
    /// and work with the Game1.cs to handle switching between clients
    /// </summary>
    class GameClient
    {
        /// <summary>
        /// The view object for our game client
        /// </summary>
        public View view;
        /// <summary>
        /// A reference to the Game1 object created in Program.cs
        /// </summary>
        public Game1 game1;

        /// <summary>
        /// Basic constructor, doesn't do anything besides save a reference to game1
        /// </summary>
        /// <param name="game1">A reference to the game1 object created in Program.cs</param>
        public GameClient(Game1 game1)
        {
            //Make sure you don't do anything here that
            //should be done in Initialize
            this.game1 = game1;
        }

        /// <summary>
        /// Called by Game1 before Initialize
        /// </summary>
        public void LoadContent()
        {

        }
        /// <summary>
        /// This is our initialize function. It is called when a GameClient class is getting ready to run. 
        /// LoadContent is called just before.
        /// </summary>
        public void Initialize()
        {
            view = new View(game1.window, Vector2.Zero, 10, 1, 0);


        }
        
        /// <param name="elapsedMils">Milliseconds elapsed since last Update</param>
        public void Update(double elapsedMils)
        {
            //REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==
            if (My.MousePress(true))
            {
                AudioHandler.PlaySoundEffect("SE/test.wav");
            }
            if (My.KeyPress(Key.Enter))
            {
                if (MusicHandler.MusicState != OpenTK.Audio.OpenAL.ALSourceState.Playing)
                    MusicHandler.PlaySong("BGM/test.wav", true, true, 240);
                else
                    MusicHandler.StopMusic();
            }
            //REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==

            view.BasicMovement(5.0f, true, true, true, true);
            view.Update();
        }

        /// <param name="elapsedMils">Milliseconds elapsed since last Render</param>
        public void Draw(double elapsedMils)
        {
            Spritebatch.Clear(Color.Black);
            Spritebatch.Begin(view);

            //REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==
            Spritebatch.DrawCircle(Vector2.Zero, 50,
                Calc.Lerp(Color.Red, Color.Yellow, (1 + Math.Sin(My.GameTime.ElapsedMilliseconds / 500f)) / 2),
                30);
            Spritebatch.DrawRectangle(new RectangleF(0, -5, 200, 10),
                Calc.Lerp(Color.Red, Color.Yellow, (1 + Math.Sin(My.GameTime.ElapsedMilliseconds / 500f)) / 2));
            Spritebatch.DrawCircle(new Vector2(150, 0), 50,
                Calc.Lerp(Color.Red, Color.Yellow, (1 + Math.Sin(My.GameTime.ElapsedMilliseconds / 500f)) / 2),
                30);
            //REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==REMOVE ME==

            Spritebatch.Draw(My.testTex, new Vector2(0, 50), new Vector2(My.testTex.Width, My.testTex.Height));

            Spritebatch.End();

            view.DrawDebug(new Vector2(0, 50), Color.White, QuickFont.QFontAlignment.Left);
        }
    }
}
